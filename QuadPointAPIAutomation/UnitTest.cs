using System;
using RestSharp;
using NUnit.Framework;
using Newtonsoft.Json.Linq;
using RestSharp.Serialization.Json;
using System.Collections.Generic;
using System.Linq;
using RestSharp.Authenticators;
using Newtonsoft.Json;
using System.IO;

[assembly: Parallelizable(ParallelScope.Fixtures)]

namespace QuadPointAPIAutomation
{
    [TestFixture]
    public class UnitTest
    {
     

        [Test]
        public void POSTWebPOSLoginByTenantNameValid200AndGETPaymentTicketByTenentNameValid200V2()
        {
            int statusCode;
            var stringJson = "{\"cashierID\":987,\"cashierPIN\":987,\"cashierPassword\":987,\"SECID\":\"8167226E-5F9A-4BBE-A56E-E864A2AD95A3\"}";
            var client = new RestClient("https://qpqa.campuscloud.io/api/v2");
            var request = new RestRequest("/tenants/Andromeda/posDevices/iSellPOSforiPhoneCPUID816/login", Method.POST);
            request.AddHeader("Content-Type", "application/json");
            request.AddJsonBody(stringJson);
            var response = client.Execute(request);
            statusCode = (int)response.StatusCode;
            JObject obs = JObject.Parse(response.Content);
            var token = obs["token"];
            Assert.AreEqual(200, statusCode, "Status code is not 200");
            Assert.That(obs["message"].ToString(), Is.EqualTo("OK"), "Post failed");
            Assert.That(obs["success"].ToString(), Is.EqualTo("True"), "Post failed");
            string QPSecCashierAuth = "{\"cashierID\":987,\"token\":\"" + token + "\",\"SECID\":\"8167226E-5F9A-4BBE-A56E-E864A2AD95A3\"}";
            var getRequest = new RestRequest("/tenants/Andromeda/posDevices/iSellPOSforiPhoneCPUID816/paymentTicket", Method.GET);
            getRequest.AddHeader("Content-Type", "application/json");
            getRequest.AddHeader("QP-Sec-CashierAuth", QPSecCashierAuth);
            var getResponse = client.Execute(getRequest);
            statusCode = (int)getResponse.StatusCode;
            var isSuccessful = getResponse.IsSuccessful;
            JObject obsGet = JObject.Parse(getResponse.Content);
            Assert.AreEqual(200, statusCode, "Status code is not 200");
            Assert.AreEqual(true, isSuccessful, "Response is not successful");
            Assert.That(obs["message"].ToString(), Is.EqualTo("OK"), "Post failed");
            Assert.That(obs["success"].ToString(), Is.EqualTo("True"), "Post failed");
        }

        [Test]
        public void POSTWebPOSLoginByTenantNameInalid400AndGETPaymentTicketByTenentNameInvalid400V2()
        {
            int statusCode;
            string statusDescription;
            var stringJson = "{\"cashierID\":987,\"cashierPIN\":222,\"cashierPassword\":987,\"SECID\":\"8167226E-5F9A-4BBE-A56E-E864A2AD95A3\"}";
            var client = new RestClient("https://qpqa.campuscloud.io/api/v2");
            var request = new RestRequest("/tenants/Andromeda/posDevices/iSellPOSforiPhoneCPUID816/login", Method.POST);
            request.AddHeader("Content-Type", "application/json");
            request.AddJsonBody(stringJson);
            var response = client.Execute(request);
            statusCode = (int)response.StatusCode;
            statusDescription = response.StatusDescription;
            JObject obs = JObject.Parse(response.Content);
            var token = obs["token"];
            Assert.AreEqual(400, statusCode, "Status code is not 400");
            Assert.AreEqual("Bad Request", statusDescription, "Status Description is not a Bad Request");
            Assert.That(obs["errors"].ToString().Contains("One of few parameters are not valid"), "Incorrect message");
            string QPSecCashierAuth = "{\"cashierID\":222,\"token\":\"" + token + "\",\"SECID\":\"8167226E-5F9A-4BBE-A56E-E864A2AD95A3\"}";
            var getRequest = new RestRequest("/tenants/Andromeda/posDevices/iSellPOSforiPhoneCPUID816/paymentTicket", Method.GET);
            getRequest.AddHeader("Content-Type", "application/json");
            getRequest.AddHeader("QP-Sec-CashierAuth", QPSecCashierAuth);
            var getResponse = client.Execute(getRequest);
            statusCode = (int)getResponse.StatusCode;
            statusDescription = getResponse.StatusDescription;
            JObject obsGet = JObject.Parse(getResponse.Content);
            Assert.AreEqual(400, statusCode, "Status code is not 400");
            Assert.AreEqual("Bad Request", statusDescription, "Status Description is not a Bad Request");
            Assert.That(obs["errors"].ToString().Contains("errorToken"), "Incorrect Message");
        }


        [Test]
        public void POSTWebPOSLoginByTenantNameInalid500AndGETPaymentTicketByTenentNameInvalid500V2()
        {
            int statusCode;
            string statusDescription;
            var client = new RestClient("https://qpqa.campuscloud.io/api/v2");
            var request = new RestRequest("/tenants/Andromeda/posDevices/iSellPOSforiPhoneCPUID816/login", Method.POST);
            request.AddHeader("Content-Type", "text/plain");
            request.AddParameter("cashierID", "987", ParameterType.GetOrPost);
            var response = client.Execute(request);
            statusCode = (int)response.StatusCode;
            statusDescription = response.StatusDescription;
            JObject obs = JObject.Parse(response.Content);
            Assert.AreEqual(500, statusCode, "Status code is not 500");
            Assert.AreEqual("Internal Server Error", statusDescription, "Status Description is not an Internal Server Error");
            Assert.That(obs["errors"].ToString().Contains("Unexpected error happened during processing the request, please try again. If the error persists, please email to  and provide the errorToken."), "Incorrect Message");
            Assert.That(obs["errors"].ToString().Contains("errorToken"), "Incorrect Message");
        }

        [Test]
        public void POSTWebPOSLoginByTenantIDValid200AndGETPaymentTicketByTenentIDValid200V2()
        {
            int statusCode;
            var stringJson = "{\"cashierID\":987,\"cashierPIN\":987,\"cashierPassword\":987,\"SECID\":\"8167226E-5F9A-4BBE-A56E-E864A2AD95A3\"}";
            var client = new RestClient("https://qpqa.campuscloud.io/api/v2");
            var request = new RestRequest("/tenants/id=101/posDevices/iSellPOSforiPhoneCPUID816/login", Method.POST);
            request.AddHeader("Content-Type", "application/json");
            request.AddJsonBody(stringJson);
            var response = client.Execute(request);
            statusCode = (int)response.StatusCode;
            JObject obs = JObject.Parse(response.Content);
            var token = obs["token"];
            Assert.AreEqual(200, statusCode, "Status code is not 200");
            Assert.That(obs["message"].ToString(), Is.EqualTo("OK"), "Post failed");
            Assert.That(obs["success"].ToString(), Is.EqualTo("True"), "Post failed");
            string QPSecCashierAuth = "{\"cashierID\":987,\"token\":\"" + token + "\",\"SECID\":\"8167226E-5F9A-4BBE-A56E-E864A2AD95A3\"}";
            var getRequest = new RestRequest("/tenants/id=101/posDevices/iSellPOSforiPhoneCPUID816/paymentTicket", Method.GET);
            getRequest.AddHeader("Content-Type", "application/json");
            getRequest.AddHeader("QP-Sec-CashierAuth", QPSecCashierAuth);
            var getResponse = client.Execute(getRequest);
            statusCode = (int)getResponse.StatusCode;
            var isSuccessful = getResponse.IsSuccessful;
            JObject obsGet = JObject.Parse(getResponse.Content);
            Assert.AreEqual(200, statusCode, "Status code is not 200");
            Assert.AreEqual(true, isSuccessful, "Response is not successful");
            Assert.That(obs["message"].ToString(), Is.EqualTo("OK"), "Post failed");
            Assert.That(obs["success"].ToString(), Is.EqualTo("True"), "Post failed");
        }


        [Test]
        public void POSTWebPOSUserLoginByTenantNameValid200V2()
        {
            int statusCode;
            var stringJson = "{\"userName\":\"qa\",\"userPassword\":\"qwerty2/\"}";
            var client = new RestClient("https://qpqa.campuscloud.io/api/v2");
            var request = new RestRequest("/tenants/Andromeda/login", Method.POST);
            request.AddHeader("Content-Type", "application/json");
            request.AddJsonBody(stringJson);
            var response = client.Execute(request);
            statusCode = (int)response.StatusCode;
            var content = response.Content;
            Assert.AreEqual(200, statusCode, "Status code is not 200");
        }

        [Test]
        public void POSTWebPOSUserLoginByTenantNameValid400V2()
        {
            int statusCode;
            var stringJson = "{\"userName\":\"qa\",\"userPassword\":\"wrongpword/\"}";
            var client = new RestClient("https://qpqa.campuscloud.io/api/v2");
            var request = new RestRequest("/tenants/Andromeda/login", Method.POST);
            request.AddHeader("Content-Type", "application/json");
            request.AddJsonBody(stringJson);
            var response = client.Execute(request);
            statusCode = (int)response.StatusCode;
            JObject obs = JObject.Parse(response.Content);
            Assert.AreEqual(400, statusCode, "Status code is not 400");
            Assert.That(obs["errors"].ToString().Contains("Invalid username or password."), "Incorrect Message");
            Assert.That(obs["errors"].ToString().Contains("errorToken"), "Incorrect Message");
        }

        [Test]
        public void POSTWebPOSUserLoginByTenantNameValid500V2()
        {
            int statusCode;
            var stringJson = "{\"userName\":\"qa\"}";
            var client = new RestClient("https://qpqa.campuscloud.io/api/v2");
            var request = new RestRequest("/tenants/Andromeda/login", Method.POST);
            request.AddHeader("Content-Type", "application/json");
            request.AddJsonBody(stringJson);
            var response = client.Execute(request);
            statusCode = (int)response.StatusCode;
            JObject obs = JObject.Parse(response.Content);
            Assert.AreEqual(500, statusCode, "Status code is not 500");
            Assert.That(obs["errors"].ToString().Contains("Unexpected error happened during processing the request, please try again. If the error persists, please email to  and provide the errorToken."), "Incorrect Message");
            Assert.That(obs["errors"].ToString().Contains("errorToken"), "Incorrect Message");
        }

        [Test]
        public void GETSyncFileByTenantIDWebPOSUserLoginByTenantIDValid200V2()
        {
            int statusCode;
            var stringJson = "{\"cashierID\":987,\"cashierPIN\":987,\"cashierPassword\":987,\"SECID\":\"8167226E-5F9A-4BBE-A56E-E864A2AD95A3\"}";
            var client = new RestClient("https://qpqa.campuscloud.io/api/v2");
            var request = new RestRequest("/tenants/id=101/posDevices/iSellPOSforiPhoneCPUID816/login", Method.POST);
            request.AddHeader("Content-Type", "application/json");
            request.AddJsonBody(stringJson);
            var response = client.Execute(request);
            statusCode = (int)response.StatusCode;
            JObject obs = JObject.Parse(response.Content);
            var token = obs["token"];
            Assert.AreEqual(200, statusCode, "Status code is not 200");
            Assert.That(obs["message"].ToString(), Is.EqualTo("OK"), "Post failed");
            Assert.That(obs["success"].ToString(), Is.EqualTo("True"), "Post failed");
            string QPSecCashierAuth = "{\"cashierID\":987,\"token\":\"" + token + "\",\"SECID\":\"8167226E-5F9A-4BBE-A56E-E864A2AD95A3\"}";
            var getRequest = new RestRequest("/tenants/id=101/posDevices/iSellPOSforiPhoneCPUID816/files/Categories", Method.GET);
            getRequest.AddHeader("QP-Sec-CashierAuth", QPSecCashierAuth);
            var getResponse = client.Execute(getRequest);
            //request.AddHeader("QP-Sec-CashierAuth", "cashierID 987, SECID 8167226E-5F9A-4BBE-A56E-E864A2AD95A3, token CF27B201E7AF44CA463A9A3D14A4D56930D6597E31808B5E9AC2556BF8F05178");
            statusCode = (int)getResponse.StatusCode;
            JObject obsGet = JObject.Parse(getResponse.Content);
            Assert.AreEqual(200, statusCode, "Status code is not 200");
            //Assert.That(obsGet["errors"].ToString().Contains("Unexpected error happened during processing the request, please try again. If the error persists, please email to  and provide the errorToken."), "Incorrect Message");
            //Assert.That(obsGet["errors"].ToString().Contains("errorToken"), "Incorrect Message");
        }




        private class User
        {
            public string email { get; set; }
            public string password { get; set; }
        }
    }
}
