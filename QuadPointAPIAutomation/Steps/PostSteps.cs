﻿using TechTalk.SpecFlow;
using System;
using RestSharp;
using NUnit.Framework;
using Newtonsoft.Json.Linq;
using QuadPointAPIAutomation.Base;
using Newtonsoft.Json;

namespace QuadPointAPIAutomation.Steps
{
    [Binding]
    class PostSteps
    {
        private Settings _settings;
        //Context Injected Step in GetPostsSteps Constructor (settings)
        public PostSteps(Settings settings) => _settings = settings;
        [Given(@"I perform the POST operation")]
        public void GivenIPerformThePOSTOperation()
        {
            _settings.RestClient = new RestClient("https://qpqa.campuscloud.io/");
            _settings.Request = new RestRequest("WebPOSService.svc/GetQPPaymentTicket", Method.POST);
            _settings.Request.AddHeader("PG_StoreNum", "137");
            _settings.Request.AddHeader("ramansteps", "t1");
            _settings.Request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
            _settings.Request.AddParameter("TenantID", "137", ParameterType.GetOrPost);
            _settings.Request.AddParameter("POSName", "Mobile Ordering Load Test", ParameterType.GetOrPost);
        }

        [When(@"I execute the request")]
        public void WhenIExecuteTheRequest()
        {
            _settings.Response = _settings.RestClient.Execute(_settings.Request);
        }


        [Then(@"I can see message as OK")]
        public void ThenICanSeeMessageAsOK()
        {
            JObject obs = JObject.Parse(_settings.Response.Content);
            Assert.That(obs["message"].ToString(), Is.EqualTo("OK"), "Post failed");
        }

        [Then(@"I can see ""(.*)"" as ""(.*)""")]
        public void ThenICanSeeAs(string key, string value)
        {
            JObject obs = JObject.Parse(_settings.Response.Content);
            Assert.That(obs["key"].ToString(), Is.EqualTo("value"), "Post failed");
        }


        [Then(@"I can see success as True")]
        public void ThenICanSeeSuccessAsTrue()
        {
            JObject obs = JObject.Parse(_settings.Response.Content);
            Assert.That(obs["success"].ToString(), Is.EqualTo("True"), "Post failed");
        }




    }

}
